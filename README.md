# Instange Generator

## INSTALLATION

### Building from source

- Have some Haskell on your system (f.e. via [ghcup](https://www.haskell.org/ghcup/)), and make sure it is up to date (```cabal update```)
- First ```git clone``` this repo. After that, there are three things you can do:
    - To try it out; use ```cabal run instance -- -n10 -m2```
    - To get the executable; use ```cabal build```. It will print out the path for you.
    - To make the executable work system-wide, use ```cabal install```. Now you can run ```instance -n10 -m2``` from anywhere.

### Download a pre-built binary executable

You can always download an automatic build for the latest commit:

*Just updated? Wait for the "build" pipeline to succeed!*

  - [x86_64-linux](https://git.science.uu.nl/info-mads/instance/-/jobs/artifacts/main/raw/dist-newstyle/build/x86_64-linux/ghc-8.10.7/instance-0.1.0.0/x/instance/build/instance/instance?job=build)

No other build targets at this moment, let me know if others are necessary.

## USAGE

Run `instance --help` to get a full list of options.

### Some Examples

  `instance`  
    Generate a random instance with default parameters.
  
  `instance -n10 -m2`  
    Generate a random instance with exactly 10 images and 2 unavailable intervals.
  
  `instance -d0`  
    Generate a random instance with only integer values for image sizes, unavailable interval start times and unavailable interval lengths.
  
  `instance --smin 0 --smax 10 -lmax 0`  
    Generate a random instance with image sizes between 0 and 10, and unavailable intervals of length 0.

  `instance -n10 > output.txt`  
    Write the instance to a file instead of stdout.
  
  `for i in {1..10}; do instance -d0 > output$i.txt; done`  
    Write 10 different random instances to the files _output1.txt_ ... _output10.txt_.
