module Instance where

import Control.Applicative (liftA3, Applicative (liftA2))
import Control.Monad (replicateM)
import Control.Arrow (Arrow(first))
import System.Random.Stateful (StatefulGen, UniformRange (uniformRM))
import Text.Printf

data Params = Params
  { n    :: !Int
  , m    :: !Int
  , d    :: !Int
  , smin :: !Float
  , smax :: !Float
  , lmin :: !Float
  , lmax :: !Float
  , tmin :: !Float
  , tmax :: !Float }
  deriving Show

data Instance = Instance
  { images    :: ![Float]
  , intervals :: ![(Float,Float)]
  , precision :: !Int }

instance Show Instance where
  show inst = unlines $ concatMap withLength [imgs, ints]
    where imgs = map showNum $ images inst
          ints = map showInterval $ intervals inst
          withLength xs = (show . fromIntegral . length) xs : xs
          showInterval (t,l) = showNum t ++ ", " ++ showNum l
          showNum = printf "%0.*f" (precision inst)

generate :: (StatefulGen g m) => Params -> g -> m Instance
generate Params{n=n, m=m, d=d, smin=smin, smax=smax, lmin=lmin, lmax=lmax, tmin=tmin, tmax=tmax}
         =  liftA3 Instance
        <$> replicateM n                      -- n images
         .  uniformRM (smin, smax)            -- between smin and smax
        <*> fmap timeslotsToIntervals
         .  replicateM m                      -- m timeslots
         .  (  liftA2 (,)
           <$> uniformRM (tmin, tmax)         -- with available between tmin and tmax
           <*> uniformRM (lmin, lmax) )       -- and unavailable between lmin and lmax
        <*> const (pure d)

-- |Convert from (available duration, unavailable duration)
-- |into the wanted format (start time, unavailable duration)
timeslotsToIntervals :: Num num => [(num, num)] -> [(num,num)]
timeslotsToIntervals = scanl1 (first . (+) . uncurry (+))




