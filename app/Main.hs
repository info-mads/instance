module Main where

import Control.Monad (join)
import Options.Applicative
import System.Random.Stateful (newStdGen, newIOGenM)

import Instance (Params (Params), generate)

paramsParser :: Parser Params
paramsParser = Params
            <$> option auto ( short 'n'   <> value 10 <> help "number of images" )
            <*> option auto ( short 'm'   <> value 2  <> help "number of unavailable intervals" )
            <*> option auto ( short 'd'   <> value 3  <> help "number of decimals (0 for integers)" )
            <*> option auto ( long "smin" <> value 0  <> help "minimum image size" )
            <*> option auto ( long "smax" <> value 10 <> help "maximum image size" )
            <*> option auto ( long "lmin" <> value 0  <> help "minimum interval length" )
            <*> option auto ( long "lmax" <> value 10 <> help "maximum interval length" )
            <*> option auto ( long "tmin" <> value 0  <> help "minimum time between intervals" )
            <*> option auto ( long "tmax" <> value 50 <> help "minimum time between intervals" )

opts = info (paramsParser <**> helper)
     (  progDesc "Create isntances for JWS Scheduling"
     <> footer "Written by Wilco Verhoef"
     <> fullDesc )

main :: IO ()
main = do p <- execParser opts
          g <- newStdGen >>= newIOGenM
          generate p g >>= print

